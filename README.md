# ANT (ANT is a Notetaking Tool) #
## Versión 1.0 (06/07/2012) Python 2.7.3 ##
### Copyleft 20/06/2012 Carlos Zayas Guggiari <carlos@carloszayas.com> ###

ANT permite almacenar y recuperar textos o archivos de prácticamente cualquier
fuente, usando una base SQLite creada en ~/ant.db (carpeta personal). Ejemplos:


```
#!bash

$ ant add Juan 555-1234                             # Tipo de dato: args
$ ant add "Linux Today" http://www.linuxtoday.com/  # Tipo de dato: args
$ ant add LoQueHayaEnElPortapapeles                 # Tipo de dato: clip
$ ant add foto.jpg                                  # Tipo de dato: file
$ mount | ant add montajes                          # Tipo de dato: pipe
```


```
#!bash

Listar datos : $ ant list [tipo_de_dato]       # Ejemplo: ant list clip
Borrar dato  : $ ant del número_identificador  # Ejemplo: ant del 5
Mostrar dato : $ ant show número_identificador # Ejemplo: ant show 3
Buscar datos : $ ant find parte_del_nombre     # Ejemplo: ant find jpg
Extraer dato : $ ant get número_identificador  # Ejemplo: ant get 7
Copiar dato  : $ ant copy número_identificador # Ejemplo: ant copy 4
Cambiar base : $ ant set nombre_de_archivo     # Ejemplo: ant set mibase.db
```

ANT fue probado en GNU/Linux, pero debería funcionar en cualquier otro UNIX.
Se recomienda copiarlo en **/usr/bin** y hacerlo ejecutable con el comando **chmod +x**

```
#!bash
$ sudo cp ant.py /usr/bin/ant
$ sudo chmod +x /usr/bin/ant
```

ANT también puede ser instalado usando el script de instalación suministrado:

```
#!bash

$ bash setup
```

Si en el equipo está instalado Apache Ant, o cualquier otro comando de nombre "ant", el instalador usará "antz" como nombre de archivo para el programa.