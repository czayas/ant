#!/usr/bin/env python
#-*- coding: utf-8 -*-

'''
ANT (ANT is a Notetaking Tool) - Versión 1.1 (02/12/2013) Python 2.7.5+
    Copyleft 20/06/2012 Carlos Zayas Guggiari <carlos@zayas.org>

ANT permite almacenar y recuperar textos o archivos de prácticamente cualquier
fuente, usando una base SQLite creada en ~/ant.db (carpeta personal). Ejemplos:

$ ant add Juan 555-1234                             # Tipo de dato: args
$ ant add "Linux Today" http://www.linuxtoday.com/  # Tipo de dato: args
$ ant add LoQueHayaEnElPortapapeles                 # Tipo de dato: clip
$ ant add foto.jpg                                  # Tipo de dato: file
$ mount | ant add montajes                          # Tipo de dato: pipe

Listar datos : $ ant list [tipo_de_dato]       # Ejemplo: ant list clip
Borrar dato  : $ ant del número_identificador  # Ejemplo: ant del 5
Mostrar dato : $ ant show número_identificador # Ejemplo: ant show 3
Buscar datos : $ ant find parte_del_nombre     # Ejemplo: ant find jpg
Extraer dato : $ ant get número_identificador  # Ejemplo: ant get 7
Copiar dato  : $ ant copy número_identificador # Ejemplo: ant copy 4
Cambiar base : $ ant set nombre_de_archivo     # Ejemplo: ant set mibase.db

ANT fue probado en GNU/Linux, pero debería funcionar en cualquier otro UNIX.
Se recomienda copiarlo en /usr/bin y hacerlo ejecutable con el comando chmod +x
'''

#-------------------------------------------------------------------------------
# Importación de módulos
#-------------------------------------------------------------------------------

import sqlite3 as sqlite
import os, string, sys; reload(sys); sys.setdefaultencoding('utf_8')
import fcntl, termios, struct
from contextlib import contextmanager

@contextmanager
def suppress_output(fd):
    if hasattr(fd, 'fileno'):
        if hasattr(fd, 'flush'): fd.flush()
        fd = fd.fileno()
    oldfd = os.dup(fd)
    try:
        devnull = os.open(os.devnull, os.O_WRONLY)
        try:
            os.dup2(devnull, fd)
        finally:
            os.close(devnull)
        yield
        os.dup2(oldfd, fd)
    finally:
        os.close(oldfd)

with suppress_output(sys.stderr): import gtk

#-------------------------------------------------------------------------------
# Declaración de variables
#-------------------------------------------------------------------------------

base = os.path.join(os.path.expanduser('~'), 'ant.db')
conf = os.path.join(os.path.expanduser('~'), '.ant')
clip = gtk.Clipboard()

cmd = ''
par = ''
err = False
clp = clip.wait_for_text()
prg = os.path.basename(sys.argv[0])

hlp = '''
Sintaxis: %s comando [argumentos]

$ %s list | add nombre | del id | show id | find texto | get id | copy id
'''

sin = '' if sys.stdin.isatty() else ''.join(sys.stdin.readlines()).strip()

if sys.stdout.isatty():
    hgt,wid = struct.unpack('hh',fcntl.ioctl(sys.stdout,termios.TIOCGWINSZ,'1234'))
else:
    hgt,wid = 0,0 # REVISAR - OJO

ESC = chr(27)  # Escape
SPC = chr(32)  # Space
BKS = chr(127) # Backspace ('\b')
TAB = '\t'     # Tab Horizontal
LF  = '\n'     # New Line Feed (chr(10))
CR  = '\r'     # Carriage Return

PRINTABLE = string.digits + string.letters + string.punctuation + SPC
TEXTCHARS = ''.join(map(chr, [7,8,9,10,12,13,27] + range(0x20, 0x100)))
ACENTUADO = 'áéíóúñäëïöüâêîôûàèìòù¿¡'; ACENTUADO += ACENTUADO.upper()
RARECHARS = '“”–—'

esbinario = lambda bytes: bool(bytes.translate(None, TEXTCHARS))

#-------------------------------------------------------------------------------

tabla = '''(
        id     integer primary key,
        nombre text,
        dato   blob,
        tipo   text,
        fecha  date
        )'''

#-------------------------------------------------------------------------------

crear     = 'create table if not exists datos ' + tabla
consultar = 'select * from datos'
contar    = 'select count(*) from datos'
mayor     = 'select max(id) from datos'
mostrar   = consultar + ' where id = ?'
buscar    = consultar + ' where nombre like ?'
borrar    = 'delete from datos where id = ?'
obtener   = 'select nombre, dato, tipo from datos where id = ?'
insertar  = '''
            insert into datos (nombre,dato,tipo,fecha)
            values (?,?,?,date("now"))
            '''

#-------------------------------------------------------------------------------
# Declaración de funciones
#-------------------------------------------------------------------------------

def esimp(dato, codec='utf8'):

    try:
        str(dato).decode(codec)
    except UnicodeDecodeError:
        log(dato)
        return False
    else:
        return True

#-------------------------------------------------------------------------------

def esbin(dato):

    binario = False

    bytes = dato[:1024] if len(dato)>1024 else dato[:]

    for byte in bytes:
        if byte not in string.printable + ACENTUADO + RARECHARS:
            binario = True
            log(byte)
            break

    return binario

#-------------------------------------------------------------------------------

def log(cadena):

    try:
        archivo = os.path.join(os.path.expanduser('~'), '.ant-log')
        logfile = open(archivo, 'a')
        try:
            logfile.write(cadena + '\n')
        finally:
            logfile.close()
    except IOError:
        pass

#-------------------------------------------------------------------------------

def tiene(lista, cadena):

    cantidad = 0

    for item in lista:
        cantidad += 1 if item.lower() in cadena.lower() else 0

    return cantidad == len(lista)

#-------------------------------------------------------------------------------
# Bloque principal
#-------------------------------------------------------------------------------

args = sys.argv[1:]

if len(args) > 0:
    cmd = args.pop(0).lower()
    if args:
        par = args.pop(0)

if os.path.isfile(conf): base = open(conf).readlines()[0].strip() # .ant

#-------------------------------------------------------------------------------

if cmd:

    con = sqlite.connect(base)
    cur = con.cursor()
    cur.execute(crear)

    may = str(cur.execute(mayor).fetchone()[0])

    #---------------------------------------------------------------------------

    if cmd == 'list':

        if par:
            if par in ['file','args','clip','pipe']:
                consultar += ' where tipo = "' + par +'"'
            else:
                err = True

        if not err:
            for row in cur.execute(consultar):
                cad = str(row[2]) if not esbin(row[2]) else '(No imprimible)'
                if LF in cad: cad = cad.strip().split(LF)[0]+'..'
                lin = str(row[0]).rjust(len(may),' ') + ' ' + row[4] + ' '
                if not par: lin += row[3] + ' '
                lin += row[1] + ': '
                lin = lin+cad if len(lin+cad) <= wid else (lin+cad)[:wid-2]+'..'
                print lin

    #---------------------------------------------------------------------------

    elif cmd == 'add' and par and all(c in PRINTABLE for c in par):

        txt = ''

        if sin:                   # Si hay algo en stdin...
            txt = sin
            tipo = 'pipe'
        elif os.path.isfile(par): # Si existe un archivo con ese nombre...
            arc = open(par,'rb')
            txt = arc.read()
            arc.close()
            tipo = 'file'
        elif args:                # Si quedan argumentos...
            txt = ' '.join(args)
            tipo = 'args'
        elif clp:                 # Si hay algo en el Portapapeles...
            txt = clp
            tipo = 'clip'
        else:
            print '%s: Nada que guardar como "%s".' % (prg,par)
            tipo = ''

        if txt:
            par = os.path.basename(par)
            cur.execute(insertar,[par,sqlite.Binary(txt),tipo])
            con.commit()

    #---------------------------------------------------------------------------

    elif cmd == 'del' and par and par.isdigit():

        cur.execute(borrar,[par])
        con.commit()

    #---------------------------------------------------------------------------

    elif cmd == 'show':

        if not par: par = may

        if par.isdigit():
            for row in cur.execute(mostrar,[par]):
                if esimp(row[2]):
                    print row[2]
                else:
                    print 'El dato "%s" (%s) no es imprimible.' % (par,row[1])

    #---------------------------------------------------------------------------

    elif cmd == 'find' and par:

        res = 0

        for row in cur.execute(buscar,['%'+par+'%']):
            if tiene(args, row[1]):
                res += 1
                num = '..(' + str(row[0]) + ')'
                dato = str(row[2]).decode('utf-8')
                cad = dato if LF not in dato else dato.strip().split(LF)[0]+num
                if len(cad)+2 > wid-len(row[1]):
                    lin = row[1] + ': '
                    msj = 'No imprimible ' + num
                    lin += msj if esimp(cad) else cad[:wid-len(lin)-len(num)] + num
                    print lin
                else:
                    print row[1] + ': ' + cad

        if res:
            print res, 'encontrado' + ('s.' if res > 1 else '.')

    #---------------------------------------------------------------------------

    elif cmd == 'get' and par and par.isdigit():

        cur.execute(obtener,[par])
        row = cur.fetchone()

        if row:
            if row[2] == 'file':
                if not os.path.exists(row[0]):
                    arc = open(row[0],'wb')
                    arc.write(row[1])
                    arc.close()
                    print 'Dato guardado en archivo "%s"' % row[0]
                else:
                    print '%s: El archivo "%s" ya existe.' % (prg,row[0])
            else:
                print row[1]
        else:
            print '%s: No existe registro con id "%s".' % (prg,par)

    #---------------------------------------------------------------------------

    elif cmd == 'copy' and par and par.isdigit():

        cur.execute(obtener,[par])
        row = cur.fetchone()

        if row:
            if not esimp(row[1]):
                clip.set_text(str(row[1]))
                clip.store()
                print 'Dato "%s" (%s) copiado al Portapapeles.' % (par,row[0])
            else:
                print 'El dato "%s" (%s) no se puede copiar.' % (par,row[0])
        else:
            print '%s: No existe registro con id "%s".' % (prg,par)

    #---------------------------------------------------------------------------

    elif cmd == 'set':

        if par:
            open(conf,'w').write(os.path.abspath(os.path.expanduser(par)))
        else:
            if os.path.isfile(conf): print open(conf).readlines()[0].strip()

    #---------------------------------------------------------------------------

    else:

        err = True

    #---------------------------------------------------------------------------

    cur.close()
    con.close()

    #---------------------------------------------------------------------------

else:

    print __doc__[1:-1]

#-------------------------------------------------------------------------------

if err: print hlp % (prg,prg)
